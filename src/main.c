#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define MAP_ANONYMOUS 0x20

static struct block_header *get_block_header(void *addr) {
    return (struct block_header *) (addr - offsetof(struct block_header, contents));
}

static void test_allocation() {
    printf("\n_____________TEST ALLOCATION_____________\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem = _malloc(0);
    assert(mem != NULL && "ALLOCATION FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("ALLOCATION SUCCESS\n");
}

static void test_malloc_and_free() {
    printf("\n__________TEST FREE AND MALLOC__________\n");
    void *heap = heap_init(0);

    void *first_block = _malloc(32);
    struct block_header *first_block_header = get_block_header(first_block);
    assert(first_block != NULL && first_block_header->capacity.bytes == 32 && "FIRST BLOCK MALLOC FAILED");
    void *second_block = _malloc(64);
    struct block_header *second_block_header = get_block_header(second_block);
    assert(second_block != NULL && second_block_header->capacity.bytes == 64 && "SECOND BLOCK MALLOC FAILED");
    void *third_block = _malloc(512);
    struct block_header *third_block_header = get_block_header(third_block);
    assert(third_block != NULL && third_block_header->capacity.bytes == 512 && "THIRD BLOCK MALLOC FAILED");
    debug_heap(stdout, heap);

    _free(second_block);
    assert(second_block_header->is_free && "SECOND BLOCK FREE FAILED");
    debug_heap(stdout, heap);

    _free(third_block);
    assert(third_block_header->is_free && "THIRD BLOCK FREE FAILED");
    _free(first_block);
    assert(first_block_header->is_free && "FIRST BLOCK FREE FAILED");
    debug_heap(stdout, heap);

    heap_term();
    printf("MALLOC AND FREE SUCCESS\n");
}


static void test_region_extension() {
    printf("\n__________TEST REGION EXTENSION__________\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *block = _malloc(16384);
    assert(block != NULL && "REGION EXTENSION FAILED");
    _free(block);
    debug_heap(stdout, heap);

    heap_term();
    printf("REGION EXTENSION SUCCESS\n");
}

static void test_region_extension_another_location() {
    printf("\n__________TEST REGION EXTENSION__________\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *blocker = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert(blocker != MAP_FAILED);
    void *first_block = _malloc(16384);
    void *second_block = _malloc(0);
    assert(get_block_header(second_block) != heap + REGION_MIN_SIZE && "REGION EXTENSION FAILED");

    debug_heap(stdout, heap);
    _free(first_block);
    _free(second_block);
    munmap(blocker, REGION_MIN_SIZE);
    heap_term();
    printf("REGION EXTENSION SUCCESS\n");
}


int main() {
    test_allocation();
    test_malloc_and_free();
    test_region_extension();
    test_region_extension_another_location();
    return 0;
}